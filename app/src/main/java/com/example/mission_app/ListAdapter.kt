package com.example.mission_app

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView

class ListAdapter internal constructor(context: Context?, states: List<Model>) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val states: List<Model> = states
    private var mItemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    fun addItemClickListener(listener: ItemClickListener) {
        mItemClickListener = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val state: Model = states[position]
        holder.flagView.setImageResource(state.flag)
        holder.nameView.text = state.title
        holder.capitalView.text = state.subtitle
        holder.hold.setOnClickListener(View.OnClickListener {
            if (mItemClickListener != null) {
                mItemClickListener!!.onItemClick(position)
            }
        })
    }

    override fun getItemCount(): Int {
        return states.size
    }

    class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val flagView: AppCompatImageView = view.findViewById<View>(R.id.image) as AppCompatImageView
        val nameView: TextView = view.findViewById<View>(R.id.title) as TextView
        val capitalView: TextView = view.findViewById<View>(R.id.description) as TextView
        val hold: RelativeLayout = view.findViewById<View>(R.id.hold) as RelativeLayout

    }

    //Define your Interface method here
    interface ItemClickListener {
        fun onItemClick(position: Int)
    }
}
