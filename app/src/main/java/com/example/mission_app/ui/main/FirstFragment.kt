package com.example.mission_app.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mission_app.R

import androidx.recyclerview.widget.RecyclerView
import com.example.mission_app.ListAdapter
import com.example.mission_app.Model


class FirstFragment : Fragment(), ListAdapter.ItemClickListener {

    private var models: ArrayList<Model> = ArrayList()

    companion object {
        fun newInstance() = FirstFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        var fragmentInflater = inflater.inflate(R.layout.main_fragment, container, false)
        val recyclerView = fragmentInflater.findViewById(R.id.list) as RecyclerView

        for (i in 1..1000) {
            models.add(Model("Title $i", "Description $i", R.drawable.baseline_http_24))
        }

        val adapter = ListAdapter(context, models)
        adapter.addItemClickListener(this);
        recyclerView.adapter = adapter
        return fragmentInflater;
    }

    override fun onItemClick(position: Int) {
        val bundle = Bundle()
        var fragment = SecondFragment.newInstance()

        bundle.putString("model_title", models[position].title)
        bundle.putString("model_desc", models[position].subtitle)

        fragment.arguments = bundle
        parentFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitNow()
    }
}