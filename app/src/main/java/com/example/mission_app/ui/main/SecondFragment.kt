package com.example.mission_app.ui.main

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.mission_app.R

class SecondFragment : Fragment()  {

    companion object {
        fun newInstance() = SecondFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val modelTitle = requireArguments().getString("model_title")
        val modelDesc = requireArguments().getString("model_desc")
        var fragmentInflater = inflater.inflate(R.layout.main_fragment2, container, false)
        var title = fragmentInflater.findViewById<View>(R.id.title) as TextView
        var description = fragmentInflater.findViewById<View>(R.id.description) as TextView

        title.text = "$modelTitle"
        description.text = "$modelDesc"
        return fragmentInflater
    }

}